__author__ = 'joe'

import pandas as pd
import sys

# file_name = sys.argv[1]

def classifier_subset(file_name, seed=123, split_ratio=.5):

    data = pd.read_csv(file_name, sep='\t', header=None, index_col=False)
    subset_pos = data[data[0] == 1].sample(frac=split_ratio, random_state=seed)
    subset_neg = data[data[0] == 0].sample(frac=split_ratio, random_state=seed)
    subset_data = pd.concat([subset_pos, subset_neg])
    out_name = '50%_' + file_name
    subset_data.to_csv(out_name, sep='\t', header=False, index=False)

    print 'Finished writing subsampled training features: {0}'.format(out_name)

    return out_name


