__author__ = 'joe'

import gensim
from gensim.models.doc2vec import TaggedDocument
from gensim.models import Doc2Vec
import gensim.models.doc2vec
import multiprocessing
from random import shuffle
import datetime
import csv
import sys

#
# corpus = sys.argv[1]
# test_corpus = sys.argv[2]
# language = sys.argv[3]
# type = sys.argv[4]
#
# dimensions = 300

# corpus = './document_corpora/arabic_subset_train_corpus.tsv'
# test_corpus = './document_corpora/arabic_test_corpus.tsv'

# language = 'english'
# type = 'new_full'
#
# corpus = '/Users/joe/Google Drive/PycharmProjects/anlp_project/sentiment-analysis/document_corpora/english_full_train_corpus.tsv'
# test_corpus = '/Users/joe/Google Drive/PycharmProjects/anlp_project/sentiment-analysis/document_corpora/english_test_corpus.tsv'
#



def infer_representations(self, x):

    return self.model.infer_vector(x.split())

def generate_features(corpus, test_corpus, language, type, dimensions=300):


    outpath = '_'.join([language,type, 'doc2vec_train_features.tsv'])

    test_out = '_'.join([language,type, 'doc2vec_test_features.tsv'])

    with open(corpus, 'rb') as f:

        all_data = csv.reader(f, delimiter='\t')
        all_docs = []

        for line in all_data:

            words = list(gensim.utils.to_unicode(line[2]).split()[0])
            tags = [line[0]]
            all_docs.append(TaggedDocument(words, tags))
        print 'finished gathering doc list'

    doc_list = all_docs[:]


    cores = multiprocessing.cpu_count()
    assert gensim.models.doc2vec.FAST_VERSION > -1, "this will be painfully slow otherwise"

    model = Doc2Vec(alpha=0.025, min_alpha=0.025, size=dimensions, workers=cores, min_count=5)

    model.build_vocab(all_docs)

    alpha, min_alpha, passes = (0.025, 0.001, 20)
    alpha_delta = (alpha - min_alpha) / passes

    print("START %s" % datetime.datetime.now())

    for epoch in range(passes):
        shuffle(doc_list)
        model.alpha, model.min_alpha = alpha, alpha
        model.train(doc_list)
        print("Finished epoch %s" % (epoch))
        alpha -= alpha_delta
    print 'finished training doc2vec model'


    for i in range(len(all_docs)):
        if all_docs[i][1] == ['12810']:
            print i

    print 'Writing training features to file'
    with open(outpath, 'wb') as output_file, open(corpus, 'rb') as input_file:

        writer = csv.writer(output_file, delimiter='\t')
        reader = csv.reader(input_file, delimiter='\t')

        for line in reader:
            row = [line[1]]
            row += model.docvecs[line[0]].tolist()
            writer.writerow(row)
    print 'Finished writing {0}'.format(outpath)




    with open(test_corpus, 'rb') as input_file, open(test_out, 'wb') as output_file:

        writer = csv.writer(output_file, delimiter='\t')
        reader = csv.reader(input_file, delimiter='\t')


        for line in reader:
            words = list(gensim.utils.to_unicode(line[2]).split()[0])
            row = [line[1]]
            row += model.infer_vector(words)
            writer.writerow(row)

    print 'Finished writing {0}'.format(test_out)


