from __future__ import division
__author__ = 'joe'

import pandas as pd
import numpy as np
import sys


"""
mandarin stats
Total Reviews - 54,003
Positive Reviews - 39,693
negative reviews - 14310

"""

language = sys.argv[1]
file_name = sys.argv[2]
# n_pos = 39693
# n_neg = 14310
seed = 123
n_pos = 39693
n_neg = 8224

#
# language = 'arabic'
# file_name = 'test_binary_arabic_reviews.tsv'
# n_pos = 39693
# n_neg = 14310
# seed = 123



def get_stratified_sample(n_pos, n_neg, file_name, seed):

    data = pd.read_csv(file_name, sep='\t', header=None, index_col=0)

    print 'Randomly sampling {0} positive cases and {1} negative cases'.format(n_pos, n_neg)
    pos_cases = data[data[1] == 1].sample(n_pos, random_state=seed)
    neg_cases = data[data[1] == 0].sample(n_neg, random_state=seed)
    stratified_sample = pd.concat([pos_cases, neg_cases])


    return stratified_sample


# np.histogram(strat_samp[1], bins = [0,1,2])


def train_test_split(data, language, seed = 123, split_ratio=.1):

    print 'Splitting data into 10% test/90% train sets'

    test_pos = data[data[1] == 1].sample(frac=split_ratio, random_state=seed)
    test_neg = data[data[1] == 0].sample(frac=split_ratio, random_state=seed)

    test_data = pd.concat([test_pos, test_neg])

    test_dist = np.histogram(test_data[1], bins = [0,1,2])
    print 'Test data distribution: {0}'.format(test_dist)
    print 'Test data count: {0}'.format(len(test_data))


    full_train_data = data.drop(test_data.index)
    full_train_dist = np.histogram(full_train_data[1], bins = [0,1,2])
    print 'Full train data distribution: {0}'.format(full_train_dist)
    print 'Full train data count: {0}'.format(len(full_train_data))

    #Get 50% subset of training data

    subset_pos = full_train_data[data[1] == 1].sample(frac=.5, random_state=seed)
    subset_neg = full_train_data[data[1] == 0].sample(frac=.5, random_state=seed)
    subset_train_data = pd.concat([subset_pos, subset_neg])
    subset_train_dist = np.histogram(subset_train_data[1], bins = [0,1,2])
    print 'Subset train data distribution: {0}'.format(subset_train_dist)
    print 'Subset train data count: {0}'.format(len(subset_train_data))

    test_name = language + '_test_corpus.tsv'
    full_train = language + '_full_train_corpus.tsv'
    subset_train = language + '_subset_train_corpus.tsv'

    print 'Saving files...\nTest data: {0}\nFull train data: {1}\nSubset train data: {2}'.format(test_name,
                                                                                                  full_train,  subset_train)

    test_data.to_csv(test_name, sep='\t', header=False, index=True)
    full_train_data.to_csv(full_train, sep='\t', header=False, index=True)
    subset_train_data.to_csv(subset_train, sep='\t', header=False, index=True)

#
# if language == 'arabic':
#
#     strat_samp = get_stratified_sample(n_pos = n_pos, n_neg = n_neg, file_name = file_name, seed = seed)
#
#     train_test_split(data=strat_samp)
#
# elif language == 'english':
#
#     data = pd.read_csv(file_name, sep='\t', header=None, index_col=0)
#     train_test_split(data=data)
#
#
# elif language == 'mandarin':
#
#     data = pd.read_csv(file_name, sep='\t', header=None, index_col=0)
#     train_test_split(data=data)
