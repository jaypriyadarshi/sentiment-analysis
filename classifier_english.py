#format : python classifier_english.py 1/2/3(unigram/bigram/trigram) path_to_training_file path_to_test_file
import nltk
import nltk.classify.util, nltk.metrics
from nltk.classify import SklearnClassifier
from sklearn.svm import LinearSVC, SVC 
from nltk import precision
from nltk import recall
from nltk import f_measure
import collections
import itertools
import sys

def gen_unigrams(tokens):
	words = nltk.FreqDist(tokens)
	unigrams = words.keys()
	return unigrams

def gen_bigrams(tokens):
	bigms = nltk.bigrams(tokens)
	freq_dist_bigrams = nltk.FreqDist(bigms)
	bi_gms = freq_dist_bigrams.keys()
	return bi_gms

def gen_trigrams(tokens):
	trigms = nltk.trigrams(tokens)
	freq_dist_trigrams = nltk.FreqDist(trigms)
	tri_gms = freq_dist_trigrams.keys()
	return tri_gms

def generate_all_features(review,argument):
	tokens = nltk.word_tokenize(review)
	#calculating uni-gram frequencies

	
	if argument == 1:
		n_grams = gen_unigrams(tokens)

	elif argument == 2:
		a = gen_unigrams(tokens)
		b = gen_bigrams(tokens)
		n_grams = a+b

	else:
		a = gen_unigrams(tokens)
		b = gen_bigrams(tokens)
		c = gen_trigrams(tokens)
		n_grams = a+b+c
	
	return n_grams

def get_all_features(content,argument):

	all_reviews = " "
	for data in content:
		data  = data.split("\t")
		#print data
		try:
			temp = data[2].replace('\n','').replace('\t','')
		except:
			temp = data[1].replace('\n','').replace('\t','')
		for i in range(len(temp)):
			if temp[i].isalpha():
				ch = temp[i].lower()
				all_reviews += ch
			if temp[i] == " ":
				all_reviews += " "
			if temp[i] == "-":
				all_reviews == "-"
			try:
				if temp[i] == ".":
					if temp[i-1] == " ":
						pass
					elif temp[i+1] == " ":
						pass
					else:
						all_reviews += " "
			except:
				pass				

	all_features = generate_all_features(all_reviews,argument)

	return all_features


def gen_train_test_features(content,all_features,argument):
	feature_set = []
	count = 1
	total = len(content)
	for data in content:
		print str((float(count)/total)*100)+"%"+"completed"
		count += 1
		data  = data.split("\t")
		#print data
		try:
			temp = data[2].replace('\n','').replace('\t','')
			rating = int(data[1])
		except:
			temp = data[1].replace('\n','').replace('\t','')
			actual_rating = int(data[0])
			if actual_rating > 3:
				rating = 1
			else:
				rating = 0
		review = ""
		temp_dict = {}
		for i in temp:
			if i.isalpha():
				i = i.lower()
				review += i
			if i == " ":
				review += i
			if i == "-":
				review += i

		n_grams = generate_all_features(review,argument)

		
		for f in all_features:
			if f in n_grams:
				temp_dict[f] = True

		if rating == 1:
			feature_set.append((temp_dict,'positive'))
		else:
			feature_set.append((temp_dict,'negative'))

	return feature_set	




arg1 = int(sys.argv[1])
train_file = sys.argv[2]
test_file = sys.argv[3]

f = open(train_file,'r')
f1 = open(test_file,'r')

content = f.readlines()
test_data = f1.readlines()

all_features = get_all_features(content,arg1)
training_set = gen_train_test_features(content,all_features,arg1)
test_set = gen_train_test_features(test_data,all_features,arg1)

print "=============================================="
print "Done generating features"
print "=============================================="

f2 = open('English_features_baseline.py','w')
f2.write("training_set = "+str(training_set)+"\n"+"test_set = "+str(test_set))
f2.close()

classifier = SklearnClassifier(LinearSVC(), sparse=False)
classifier.train(training_set)
refsets = collections.defaultdict(set)
testsets = collections.defaultdict(set)

for i, (feature, label) in enumerate(test_set):
	refsets[label].add(i)
	observed = classifier.classify(feature)
	testsets[observed].add(i)

accuracy = nltk.classify.util.accuracy(classifier, test_set)

positive_precision =precision(refsets['positive'],testsets['positive'])
positive_recall = recall(refsets['positive'],testsets['positive'])
positive_f = f_measure(refsets['positive'],testsets['positive'])

negative_precision = precision(refsets['negative'],testsets['negative'])
negative_recall = recall(refsets['negative'],testsets['negative'])
negative_f = f_measure(refsets['negative'],testsets['negative'])

print "accuracy:",accuracy
print "precision:",(positive_precision+negative_precision)/2
print "recall:",(positive_recall+negative_recall)/2
print "f-measure:",(positive_f+negative_f)/2

f.close()
f1.close()	


