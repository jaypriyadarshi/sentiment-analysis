from bs4 import BeautifulSoup
import urllib2
import itertools
import random
import urlparse
import sys
#Positive Reviews
#39693
#Total Reviews
#54003
f1 = open ('bookIds_ow_magazine.txt', 'w')
f2 = open ('bookReviews_ow_magazine.txt', 'w')

bookIds = []
#inputURL = "https://read.douban.com/ebooks/tag/%E5%B0%8F%E8%AF%B4/"
#inputURL = "https://read.douban.com/ebooks/tag/%E5%8E%86%E5%8F%B2/"
#inputURL = "https://read.douban.com/ebooks/tag/%E7%A7%91%E5%B9%BB/"
#inputURL = "https://read.douban.com/ebooks/tag/%E9%9D%92%E6%98%A5/"
#inputURL = "https://read.douban.com/ebooks/tag/%E6%8E%A8%E7%90%86/"
#inputURL = "https://read.douban.com/ebooks/tag/%E6%96%87%E5%8C%96/"
#inputURL = "https://read.douban.com/ebooks/tag/%E5%BF%83%E7%90%86%E5%AD%A6/"
#inputURL = "https://read.douban.com/tag/%E5%B0%8F%E8%AF%B4/"
#inputURL = "https://read.douban.com/tag/%E7%A7%91%E5%B9%BB/"
#inputURL = "https://read.douban.com/tag/%E8%AF%97%E6%AD%8C/"
#inputURL = "https://read.douban.com/tag/%E6%97%85%E8%A1%8C/"
#inputURL = "https://read.douban.com/tag/%E9%9A%8F%E7%AC%94/"
#inputURL = "https://read.douban.com/tag/%E6%95%A3%E6%96%87/"
#inputURL = "https://read.douban.com/tag/%E6%96%87%E5%8C%96/"
#inputURL = "https://read.douban.com/tag/%E5%B9%BB%E6%83%B3/"
#inputURL = "https://read.douban.com/tag/%E7%94%BB%E5%86%8C/"
inputURL = "https://read.douban.com/tag/%E7%AC%AC%E4%B8%89%E5%B1%8A%E5%BE%81%E6%96%87%E5%A4%A7%E8%B5%9B/"

res = urllib2.urlopen(inputURL)
html_code = res.read()
soup = BeautifulSoup(html_code, 'html.parser')
bookList = soup.find("ul", {"class": "list-lined ebook-list column-list"})
bookItems = bookList.find_all("li", {"class": "item"})
print len(bookItems)
for book in bookItems:
	bookDiv = book.find("div", {"class": "title"})
	bookIds.append(str(book.find("a").get("href").split("/")[2]))
	f1.write((bookDiv.find("a").contents[0]).encode("utf-8"))
	f1.write("	")
	f1.write(bookIds[-1] + "\n")

i = 20
while i <= 300:
	print "==================================================================================="
	print i
	#inputURL = "https://read.douban.com/ebooks/tag/%E5%B0%8F%E8%AF%B4/?cat=book&sort=top&start=" + str(i)
	inputURL = "https://read.douban.com/tag/%E7%AC%AC%E4%B8%89%E5%B1%8A%E5%BE%81%E6%96%87%E5%A4%A7%E8%B5%9B/?cat=article&sort=top&start=" + str(i)
	res = urllib2.urlopen(inputURL)
	html_code = res.read()
	soup = BeautifulSoup(html_code, 'html.parser')
	bookList = soup.find("ul", {"class": "list-lined ebook-list column-list"})
	bookItems = bookList.find_all("li", {"class": "item"})
	print len(bookItems)
	for book in bookItems:
		bookDiv = book.find("div", {"class": "title"})
		bookIds.append(str(book.find("a").get("href").split("/")[2]))
		f1.write((bookDiv.find("a").contents[0]).encode("utf-8"))
		f1.write("	")
		f1.write(bookIds[-1] + "\n")

	i += 20

for id in bookIds:
	inputURL = "https://read.douban.com/ebook/" + id + "/reviews?sort=score"
	res = urllib2.urlopen(inputURL)
	html_code = res.read()
	soup = BeautifulSoup(html_code, 'html.parser')
	#print(soup.prettify())
	i = 0
	descDiv = soup.find_all("div", {"class": "desc"})
	ratingSpan = soup.find_all("span", {"itemprop": "reviewRating"})
	bookDiv = soup.find("div", {"class": "title"})
	bookTitle = bookDiv.find("h2")
	print bookTitle
	try:
		for index in range(len(descDiv)):
			print "=============="
			print i
			i += 1
			f2.write(id + "		")
			f2.write(((descDiv[index].contents)[0]).encode("utf-8") + "	")
			f2.write(ratingSpan[index].find_all("meta", {"itemprop": "bestRating"})[0].get('content') + "	")
			f2.write(ratingSpan[index].find_all("meta", {"itemprop": "worstRating"})[0].get('content') + "	")
			f2.write(ratingSpan[index].find_all("meta", {"itemprop": "ratingValue"})[0].get('content') + "\n")
	except:
		if (descDiv[index].contents) == "":
			continue
	
'''
with open(f, 'r') as openfileobject:
		for id in openfileobject:
			id = id.rstrip("\n").split("	")
			id = id[-1]
			print id
			inputURL = "https://read.douban.com/ebook/" + id + "/reviews?sort=score"
			res = urllib2.urlopen(inputURL)
			html_code = res.read()
			soup = BeautifulSoup(html_code, 'html.parser')
			#print(soup.prettify())
			i = 0
			descDiv = soup.find_all("div", {"class": "desc"})
			ratingSpan = soup.find_all("span", {"itemprop": "reviewRating"})
			bookDiv = soup.find("div", {"class": "title"})
			bookTitle = bookDiv.find("h2")
			print bookTitle
			try:
				for index in range(len(descDiv)):
					print "=============="
					print i
					i += 1
					f2.write(id + "		")
					f2.write(((descDiv[index].contents)[0]).encode("utf-8") + "	")
					f2.write(ratingSpan[index].find_all("meta", {"itemprop": "bestRating"})[0].get('content') + "	")
					f2.write(ratingSpan[index].find_all("meta", {"itemprop": "worstRating"})[0].get('content') + "	")
					f2.write(ratingSpan[index].find_all("meta", {"itemprop": "ratingValue"})[0].get('content') + "\n")
			except:
				if (descDiv[index].contents) == "":
					continue

'''