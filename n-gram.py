import nltk

f = open('English_Reviews_latest.txt','r')
f1 = open('uni_gram_statistics.py','w')
f2 = open('bi_gram_statistics.py','w')
f3 = open('tri_gram_statistics.py','w')
content = f.readlines()

#unigram, bigram and trigram statistics
ug_statistics = {}
bg_statistics = {}
tg_statistics = {}

for data in content:
	data  = data.split("\t")
	temp = data[1].replace('\n','').replace('\t','')
	review = ""
	for i in temp:
		if i.isalpha():
			i = i.lower()
			review += i
		if i == " ":
			review += i

	tokens = nltk.word_tokenize(review)
	#calculating uni-gram frequencies
	for t in tokens:
		if t in ug_statistics:
			ug_statistics[t] += 1
		else:
			ug_statistics[t] = 1 

	#calculating bi-gram frequencies
	bi_gms = nltk.bigrams(tokens)
	freq_dist_bigrams = nltk.FreqDist(bi_gms)
	for i,j in freq_dist_bigrams.items():
		if i[0] in bg_statistics:
			if i[1] in bg_statistics[i[0]]:
				bg_statistics[i[0]][i[1]] += j
			else:
				bg_statistics[i[0]][i[1]] = j
		else:
			bg_statistics[i[0]] = {}
			bg_statistics[i[0]][i[1]] = j

	#calculating tri-gram frequencies
	tri_gms = nltk.trigrams(tokens)
	freq_dist_trigrams = nltk.FreqDist(tri_gms)
	for i,j in freq_dist_trigrams.items():
		if i[0] in tg_statistics:
			if i[1] in tg_statistics[i[0]]:
				if i[2] in tg_statistics[i[0]][i[1]]:
					tg_statistics[i[0]][i[1]][i[2]] += j
				else:
					tg_statistics[i[0]][i[1]][i[2]] = j
			else:
				tg_statistics[i[0]][i[1]] = {}
				tg_statistics[i[0]][i[1]][i[2]] = j
		else:
			tg_statistics[i[0]] = {}
			tg_statistics[i[0]][i[1]] = {}
			tg_statistics[i[0]][i[1]][i[2]] = j

f1.write("ug_statistics = "+str(ug_statistics))
f2.write("bg_statistics = "+str(bg_statistics))
f3.write("tg_statistics = "+str(tg_statistics))

f.close()
f1.close()
f2.close()
f3.close()
	

